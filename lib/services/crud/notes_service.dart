import 'dart:async';

import 'package:flutter/cupertino.dart';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/foundation.dart';

import 'crud_exceptions.dart';

class NotesService {
  Database? _db;
  //keep a list of notes locally (cache the notes)
  List<DatabaseNote> _notes = [];

  final _notesStreamController =
      StreamController<List<DatabaseNote>>.broadcast();

//this allows us to subscribe to the notes stream in our UI
  Stream<List<DatabaseNote>> get allNotes => _notesStreamController.stream;

  //singleton for notes service
  static final NotesService _shared = NotesService._sharedInstance();
  NotesService._sharedInstance();
  factory NotesService() => _shared;

  Future<DataBaseUser> getOrCreateUser({required String email}) async {
    try {
      final user = await getUser(email: email);
      return user;
    } on NoUserFoundException {
      final createdUser = await createUser(email: email);
      return createdUser;
    } catch (e) {
      rethrow;
    }
  }

  Future<void> cacheNotes() async {
    final allNotes = await getAllNotes();
    _notes = allNotes.toList();
    _notesStreamController.add(_notes);
  }

  Future<DatabaseNote> updateNote(
      {required DatabaseNote note, required String text}) async {
    await ensureDBisOpen();
    final db = _getDatabaseOrThrow();
    //make sure note exists
    await getNote(note.id);
    final noteCount = await db.update(noteTable, {
      noteColumn: text,
      isSyncToCloudColumn: 0,
    });
    if (noteCount == 0) {
      throw NoteNotFoundException();
    }
    final updatedNote = await getNote(note.id);
    _notes.removeWhere((element) => element.id == updatedNote.id);
    _notes.add(updatedNote);
    _notesStreamController.add(_notes);
    return updatedNote;
  }

  Future<Iterable<DatabaseNote>> getAllNotes() async {
    await ensureDBisOpen();
    final db = _getDatabaseOrThrow();
    final notes = await db.query(noteTable);
    return notes.map((note) => DatabaseNote.fromRow(note));
  }

  Future<DatabaseNote> getNote(int id) async {
    await ensureDBisOpen();
    final db = _getDatabaseOrThrow();
    final row = await db.query(
      noteTable,
      limit: 1,
      where: 'id = ?',
      whereArgs: [id],
    );
    if (row.isEmpty) {
      throw NoteNotFoundException();
    }
    final count = DatabaseNote.fromRow(row.first);

    _notes.removeWhere((element) => element.id == id);
    _notes.add(count);
    _notesStreamController.add(_notes);

    return count;
  }

  Future<int> deleteAllNotes() async {
    await ensureDBisOpen();
    final db = _getDatabaseOrThrow();
    final deleteCount = db.delete(noteTable);
    _notes = [];
    _notesStreamController.add(_notes);
    return deleteCount;
  }

  Future<void> deleteNote({required int id}) async {
    await ensureDBisOpen();
    final db = _getDatabaseOrThrow();
    //deleting in database
    final noteId = await db.delete(
      noteTable,
      where: 'id = ?',
      whereArgs: [id],
    );
    if (noteId == 0) {
      throw UnableToDeleException();
    }
    _notes.removeWhere((note) => note.id == id);
    _notesStreamController.add(_notes);
  }

  Future<DatabaseNote> createNote({required DataBaseUser owner}) async {
    await ensureDBisOpen();
    final db = _getDatabaseOrThrow();
    //check user in database
    final user = await getUser(email: owner.email);
    if (user != owner) {
      throw NoUserFoundException();
    }
    const text = '';
    int id = await db.insert(noteTable, {
      userIDColumn: owner.id,
      textColumn: text,
      isSyncToCloudColumn: 1,
    });

    final note = DatabaseNote(
      id: id,
      userID: owner.id,
      text: text,
      isSyncToCloud: false,
    );
    _notes.add(note);
    _notesStreamController.add(_notes);
    return note;
  }

  Future<DataBaseUser> getUser({required String email}) async {
    await ensureDBisOpen();
    final db = _getDatabaseOrThrow();
    final user = await db.query(
      userTable,
      limit: 1,
      where: 'email = ?',
      whereArgs: [email.toLowerCase()],
    );
    if (user.isEmpty) {
      throw NoUserFoundException();
    }
    final id = DataBaseUser.fromRow(user.first).id;
    return DataBaseUser(id: id, email: email);
  }

  Future<DataBaseUser> createUser({required String email}) async {
    await ensureDBisOpen();
    final db = _getDatabaseOrThrow();
    final results = await db.query(
      userTable,
      limit: 1,
      where: 'email = ?',
      whereArgs: [email.toLowerCase()],
    );
    if (results.isNotEmpty) {
      throw UserExistsException();
    }
    final userID =
        await db.insert(userTable, {emailColumn: email.toLowerCase()});

    return DataBaseUser(id: userID, email: email);
  }

  Future<void> deleteUser({required String email}) async {
    await ensureDBisOpen();
    final db = _getDatabaseOrThrow();
    int num = await db.delete(
      userTable,
      where: 'email = ?',
      whereArgs: [email.toLowerCase()],
    );
    if (num != 1) {
      throw CouldNotDeleteException();
    }
  }

  Database _getDatabaseOrThrow() {
    final db = _db;
    if (db == null) {
      throw DataBaseClosedException();
    }
    return db;
  }

  Future<void> ensureDBisOpen() async {
    try {
      await open();
    } on DataBaseAlreadyOpenException {
      //do something
    }
  }

  Future<void> close() async {
    if (_db == null) {
      throw CouldNotCloseException();
    } else {
      await _db!.close();
      _db = null;
    }
  }

  Future<void> open() async {
    if (_db != null) {
      throw DataBaseAlreadyOpenException();
    }
    try {
      //every system has its own documents dir
      //this allows us to get the dir
      final docsPath = await getApplicationDocumentsDirectory();
      //join our dir with the db name
      final dbPath = join(docsPath.path, dbname);
      final db = await openDatabase(dbPath);
      _db = db;
      //create the user table
      db.execute(createuserTable);
      //create the notes table
      db.execute(createNotesTable);
      await cacheNotes();
    } catch (e) {
      throw CouldNotOpenException();
    }
  }
}

class DatabaseNote {
  final int id;
  final int userID;
  final String text;
  final bool isSyncToCloud;

  DatabaseNote({
    required this.id,
    required this.userID,
    required this.text,
    required this.isSyncToCloud,
  });

  DatabaseNote.fromRow(Map<String, Object?> map)
      : id = map[idColumn] as int,
        userID = map[userIDColumn] as int,
        text = map[noteColumn] as String,
        isSyncToCloud = (map[isSyncToCloudColumn] as int) == 1 ? true : false;

  @override
  String toString() {
    return '''
    Person ID =  $id 
    Person userID = $userID
    Person isSyncToCloud = $isSyncToCloud
    person Note = $text
        ''';
  }

  @override
  bool operator ==(covariant DataBaseUser other) => id == other.id;

  @override
  int get hashCode => id.hashCode;
}

@immutable
class DataBaseUser {
  //class is used to translate
  //DB into dart code
  final int id;
  final String email;

  const DataBaseUser({required this.id, required this.email});

//represents a row in the user table
  DataBaseUser.fromRow(Map<String, Object?> map)
      : id = map[idColumn] as int,
        email = map[emailColumn] as String;

  @override
  String toString() {
    return '''
    Person ID =  $id 
    Person email = $email
    ''';
  }

  //how do we check the user is not the same
  //use coveriant to check not on a class level but a level u specify
  //to compare equality
  //comparing ids
  @override
  bool operator ==(covariant DataBaseUser other) => id == other.id;

  @override
  int get hashCode => id.hashCode;
}

const idColumn = 'id';
const emailColumn = 'email';
const userIDColumn = 'user_id';
const noteColumn = 'text';
const isSyncToCloudColumn = 'is_sync_to_cloud';
const dbname = 'notes.db';
const noteTable = 'note';
const userTable = 'user';
const textColumn = 'text';
const createuserTable = '''
CREATE TABLE IF NOT EXISTS "user " (
	"id"	INTEGER NOT NULL,
	"email"	TEXT NOT NULL UNIQUE,
	PRIMARY KEY("id" AUTOINCREMENT)
);

''';

const createNotesTable = '''
CREATE IF NOT EXISTS TABLE "notes " (
	"id"	INTEGER NOT NULL,
	"user_id"	INTEGER NOT NULL,
	"note"	TEXT,
	"is_sync_to_cloud"	INTEGER NOT NULL DEFAULT 0,
	FOREIGN KEY("user_id") REFERENCES "user "("id"),
	PRIMARY KEY("id" AUTOINCREMENT)
);
''';
