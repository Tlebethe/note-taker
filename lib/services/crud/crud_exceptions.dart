class DataBaseAlreadyOpenException implements Exception {}

class CouldNotOpenException implements Exception {}

class CouldNotCloseException implements Exception {}

class DataBaseClosedException implements Exception {}

class CouldNotDeleteException implements Exception {}

class UserExistsException implements Exception {}

class NoUserFoundException implements Exception {}

class UnableToDeleException implements Exception {}

class NoteNotFoundException implements Exception {}
