import 'package:firebase_auth/firebase_auth.dart' show User;
import 'package:flutter/foundation.dart';

@immutable
class AuthUser {
  final bool isVerified;
  final String? email;

  const AuthUser({required this.isVerified, required this.email});

  //creates a factory
  //sets is verified here

  //custom firebase auth
  factory AuthUser.fromFirebase(User user) => AuthUser(
        isVerified: user.emailVerified,
        email: user.email,
      );
}
