import 'package:note_taker/services/auth/auth_user.dart';

abstract class AuthProvider {
  //get current user
  AuthUser? get getCurrentUser;

//login a user
  Future<AuthUser> login({
    required String email,
    required String password,
  });

  //signout a user
  Future<void> logOut();

  //send email verification
  Future<void> sendEmailVerification();

  Future<AuthUser> register({
    required String email,
    required String password,
  });

  //initalize firebase
  Future<void> initialize();
}
