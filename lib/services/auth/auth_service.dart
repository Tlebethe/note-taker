import 'package:note_taker/services/auth/auth_provider.dart';
import 'package:note_taker/services/auth/auth_user.dart';
import 'package:note_taker/services/auth/firebase_auth_provider.dart';

class AuthService implements AuthProvider {
  final AuthProvider provider;

  factory AuthService.firebase() => AuthService(FireBaseAuthProvider());

  AuthService(this.provider);

  @override
  AuthUser? get getCurrentUser => provider.getCurrentUser;

  @override
  Future<void> logOut() => provider.logOut();

  @override
  Future<AuthUser> login({required String email, required String password}) =>
      provider.login(email: email, password: password);

  @override
  Future<AuthUser> register(
          {required String email, required String password}) =>
      provider.register(email: email, password: password);

  @override
  Future<void> sendEmailVerification() => provider.sendEmailVerification();

  @override
  Future<void> initialize() => provider.initialize();
}
