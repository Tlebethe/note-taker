import 'package:flutter/material.dart';
import 'package:note_taker/constants/routes.dart';
import 'package:note_taker/screens/notes/new_note_view.dart';
import 'package:note_taker/screens/notes/notes_view.dart';
import 'package:note_taker/screens/login.dart';
import 'package:note_taker/screens/register.dart';
import 'package:note_taker/screens/verify_email.dart';
import 'package:note_taker/services/auth/auth_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(
    MaterialApp(
      title: 'Taking notes',
      home: const MyApp(),
      routes: {
        loginRoute: (context) => const LoginView(),
        registerRoute: (context) => const RegisterView(),
        notesViewRoute: (context) => const NotesView(),
        verifyEmailRoute: (context) => const VerifyEmailView(),
        newNoteViewRoute: (context) => const NewNoteView(),
      },
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MyHomePage();
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: AuthService.firebase().initialize(),
        builder: ((context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.done:
              final user = AuthService.firebase().getCurrentUser;

              if (user != null) {
                if (user.isVerified) {
                  return const NotesView();
                } else {
                  return const VerifyEmailView();
                }
              } else {
                return const LoginView();
              }

            default:
              return const CircularProgressIndicator();
          }
        }));
  }
}
