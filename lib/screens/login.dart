import 'package:flutter/material.dart';
import 'package:note_taker/constants/routes.dart';
import 'package:note_taker/services/auth/auth_exceptions.dart';
import 'package:note_taker/services/auth/auth_service.dart';

import '../utilites/show_error_dialog.dart';

class LoginView extends StatefulWidget {
  const LoginView({super.key});

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  late TextEditingController _emailController;
  late TextEditingController _passwordController;

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Login')),
      body: Column(
        children: [
          TextField(
            controller: _emailController,
            decoration: const InputDecoration(
              hintText: 'Enter Email',
            ),
          ),
          TextField(
            controller: _passwordController,
            decoration: const InputDecoration(
              hintText: 'Enter Password',
            ),
          ),
          TextButton(
            onPressed: () async {
              final email = _emailController.text;
              final password = _passwordController.text;

              try {
                await AuthService.firebase().login(
                  email: email,
                  password: password,
                );

                //if user is verified
                final user1 = AuthService.firebase().getCurrentUser;

                if (user1?.isVerified ?? false) {
                  if (mounted) {
                    Navigator.of(context).pushNamedAndRemoveUntil(
                      notesViewRoute,
                      (route) => false,
                    );
                  }
                } else {
                  //if user is not verified
                  if (mounted) {
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        verifyEmailRoute, (route) => false);
                  }
                }
              } on GenericAuthException catch (e) {
                await showErrorDialog(context, e.toString());
              } catch (e) {
                showErrorDialog(context, e.toString());
              }
            },
            child: const Text('Login'),
          ),
          TextButton(
            onPressed: () {
              Navigator.of(context)
                  .pushNamedAndRemoveUntil(registerRoute, (route) => false);
            },
            child: const Text('Not Registered? Register here!'),
          ),
        ],
      ),
    );
  }
}
