import 'package:flutter/material.dart';
import 'package:note_taker/constants/routes.dart';
import 'package:note_taker/services/auth/auth_service.dart';
import 'package:note_taker/services/crud/notes_service.dart';

import '../../enums/menu_action.dart';

class NotesView extends StatefulWidget {
  const NotesView({super.key});

  @override
  State<NotesView> createState() => _NotesViewState();
}

class _NotesViewState extends State<NotesView> {
  late final NotesService _notesService;
  String get userEmail => AuthService.firebase().getCurrentUser!.email!;
  PopUpMenuItem? selectedMenu;

  @override
  void initState() {
    _notesService = NotesService();
    super.initState();
  }

  @override
  void dispose() {
    _notesService.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Your Notes'),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context).pushNamed(newNoteViewRoute);
            },
            icon: const Icon(Icons.add),
          ),
          PopupMenuButton<PopUpMenuItem>(
            initialValue: selectedMenu,
            onSelected: ((value) async {
              switch (value) {
                case PopUpMenuItem.logout:
                  final shouldLogOut = _showMyDialog(context);
                  if (await shouldLogOut ?? false) {
                    AuthService.firebase().logOut();
                    Navigator.of(context)
                        .pushNamedAndRemoveUntil(loginRoute, (_) => false);
                  }
              }
            }),
            itemBuilder: ((context) {
              return [
                const PopupMenuItem<PopUpMenuItem>(
                  value: PopUpMenuItem.logout,
                  child: Text('Logout'),
                ),
              ];
            }),
          ),
        ],
      ),
      body: FutureBuilder(
        future: _notesService.getOrCreateUser(email: userEmail),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.done:
              return StreamBuilder(
                stream: _notesService.allNotes,
                builder: ((context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                    case ConnectionState.active:
                      return const Text('waiting for data');
                    default:
                      return const CircularProgressIndicator();
                  }
                }),
              );

            default:
              return const CircularProgressIndicator();
          }
        },
      ),
    );
  }
}

Future<bool?> _showMyDialog(BuildContext context) async {
  return showDialog<bool>(
    context: context,
    barrierDismissible: true, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('Sign Out'),
        content: const Text('Are you sure you want to sign out?'),
        actions: <Widget>[
          TextButton(
            child: const Text('Cancel'),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),
          TextButton(
            child: const Text('Sign Out'),
            onPressed: () {
              Navigator.of(context).pop(true);
            },
          ),
        ],
      );
    },
  ).then((value) => value);
}
