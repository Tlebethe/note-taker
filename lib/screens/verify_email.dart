import 'package:flutter/material.dart';
import 'package:note_taker/constants/routes.dart';
import 'package:note_taker/services/auth/auth_service.dart';

class VerifyEmailView extends StatefulWidget {
  const VerifyEmailView({super.key});

  @override
  State<VerifyEmailView> createState() => _VerifyEmailViewState();
}

class _VerifyEmailViewState extends State<VerifyEmailView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Verify Email'),
        actions: [
          TextButton(
            onPressed: () {
              AuthService.firebase().logOut();
              Navigator.of(context)
                  .pushNamedAndRemoveUntil(loginRoute, (route) => false);
            },
            child: const Icon(
              Icons.restart_alt,
              color: Colors.white,
            ),
          )
        ],
      ),
      body: Column(
        children: [
          const Text('Did not get veification email'),
          const Text('Resend email address.'),
          ElevatedButton(
              onPressed: () {
                AuthService.firebase().sendEmailVerification;
                debugPrint('resending email verification');
                Navigator.of(context)
                    .pushNamedAndRemoveUntil(loginRoute, (route) => false);
              },
              child: const Text('Send verification'))
        ],
      ),
    );
  }
}
