import 'package:flutter/material.dart';
import 'package:note_taker/constants/routes.dart';
import 'package:note_taker/services/auth/auth_exceptions.dart';
import 'package:note_taker/services/auth/auth_service.dart';
import 'package:note_taker/utilites/show_error_dialog.dart';

class RegisterView extends StatefulWidget {
  const RegisterView({super.key});

  @override
  State<RegisterView> createState() => _RegisterView();
}

class _RegisterView extends State<RegisterView> {
  late TextEditingController _emailController;
  late TextEditingController _passwordController;

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Register')),
      body: Column(
        children: [
          TextField(
            keyboardType: TextInputType.emailAddress,
            controller: _emailController,
            decoration: const InputDecoration(
              hintText: 'Enter Email',
            ),
          ),
          TextField(
            controller: _passwordController,
            decoration: const InputDecoration(
              hintText: 'Enter Password',
            ),
          ),
          ElevatedButton(
              onPressed: () async {
                String email = _emailController.text;
                String password = _passwordController.text;
                try {
                  await AuthService.firebase()
                      .register(email: email, password: password);
                  //confirm user email
                  if (mounted) {
                    Navigator.of(context).pushNamed(verifyEmailRoute);
                    //final user = FirebaseAuth.instance.currentUser;
                    await AuthService.firebase().sendEmailVerification();

                    debugPrint('email verification sent');
                  }
                } on EmailAlreadyInUserException {
                  showErrorDialog(context, 'Email is in use');
                } on GenericAuthException {
                  showErrorDialog(context, 'error occured');
                } catch (e) {
                  showErrorDialog(context, e.toString());
                }
              },
              child: const Text('Register')),
          TextButton(
              onPressed: () {
                Navigator.of(context)
                    .pushNamedAndRemoveUntil(loginRoute, (route) => false);
              },
              child: const Text('Registererd ? Login Here'))
        ],
      ),
    );
  }
}
